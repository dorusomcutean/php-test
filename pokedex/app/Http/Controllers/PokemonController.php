<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

class PokemonController extends Controller
{   
    protected $client;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->client = new Client(['base_uri' => 'https://pokeapi.co/api/v2/']);
    }

    /**
     * Show list of pokemons
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $response = $this->client->request('GET', 'pokemon?offset=20&limit=20');

        if($response->getStatusCode() != 200) {
            return view('dashboard', ['error' => 'Something went wrong. Please try again later.']);
        }

        $dataDecoded = json_decode($response->getBody());

        $pokemons = [
            'totalCount'    => $dataDecoded->count,
            'resultsCount'  => count($dataDecoded->results),
            'results'       => $dataDecoded->results,
        ];

        return view('dashboard', $pokemons);
    }

    /**
     * Show a specific pokemon
     * 
     * @return \Illuminate\View\View
     */
    public function show(String $name)
    {
        if(is_string($name) === false) {
            return view('pokemon', ['error' => 'Pokémon Name must be a string.']);
        }
    
        try {
            $response = $this->client->request('GET', 'pokemon/' . mb_strtolower($name));

            if($response->getStatusCode() != 200) {
                return view('pokemon', ['error' => 'Something went wrong. Please try again later.']);
            }

            $dataDecoded = json_decode($response->getBody()->getContents());

            $pokemon = [
                'height'    => $dataDecoded->height,
                'weight'    => $dataDecoded->weight,
                'name'      => $dataDecoded->name,
                'species'   => $dataDecoded->species,
                'abilities' => $dataDecoded->abilities,
                'image'     => $dataDecoded->sprites->front_default,
            ];

            return view('pokemon', $pokemon);
        } catch (\Exception $e) {
            return view('pokemon', ['error' => 'Something went wrong. Please try again later.']);
        }
    }

    /**
     * Search for specific pokemon
     */
    public function search(Request $request) 
    {
        $this->validate($request, [
            'name' => 'required|string',
        ]);

        return $this->show($request->input('name')); 
    }
}
