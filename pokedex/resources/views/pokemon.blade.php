@extends('layouts.app', ['pageSlug' => 'dashboard'])

@section('content')
    <div class="row">
        @if(isset($error))
    <h3>{{$error}}</h3>
        
        @else
        <div class="col-md-12">
            <h2>Pokémon</h2>
          </div>
          <div class="col-md-12">
                <div class="card card-plain">
                    <div class="card-body">
                        <div>
                          <table class="table table-hover">
                            <thead>
                              <th>
                                Name
                              </th>
                              <th>
                                Height
                              </th>
                              <th>
                                Weight
                              </th>
                              <th>
                                Species
                              </th>
                              <th>
                                Abilities
                              </th>
                              <th>
                                  Image (front_default)
                              </th>
                            </thead>
                            <tbody>
                              <tr>
                                <td>
                                    {{$name}}
                                </td>
                                <td>
                                    {{$height}}
                                </td>
                                <td>
                                    {{$weight}}
                                </td>
                                <td>
                                  <a href="{{$species->url}}">{{$species->name}}</a>
                                </td>
                                <td>
                                  <ul>  
                                    @foreach($abilities as $ability)
                                  <li><a href="{{$ability->ability->url}}">{{$ability->ability->name}}</a></li>
                                  @endforeach
                                  </ul>
                                </td>
                                <td>
                                    <img src="{{$image}}" alt="image"/>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                </div>
            </div>
            @endif
    </div>
@endsection
