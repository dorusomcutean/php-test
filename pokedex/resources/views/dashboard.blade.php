@extends('layouts.app', ['pageSlug' => 'dashboard'])

@section('content')
    <div class="row">
      <div class="col-md-12">
          <h2> Pokémons</h2>
          <div class="col-md-12">
                  <form action="/search" method="POST" role="search">
                      {{ csrf_field() }}
                      <div class="input-group">
                          <input type="text" class="form-control" name="name"
                              placeholder="Search"> <span class="input-group-btn">
                              <button type="submit" class="btn btn-default">
                                  Search
                              </button>
                          </span>
                      </div>
                  </form>
          </div>
          <div class="card card-plain">
            @if($resultsCount > 0)
            <div class="card-header card-header-primary">
            <p class="card-category"> {{$resultsCount}}</b> of <b>{{$totalCount}}</b> results</p>
            </div>
            <div class="card-body">
              <div>
                <table class="table table-hover">
                  <thead class="">
                    <th>
                      Name
                    </th>
                    <th>
                    </th>
                  </thead>
                  <tbody>
                    @foreach ($results as $pokemon)
                    <tr>
                      <td>
                        <a href="/{{$pokemon->name}}">{{$pokemon->name}}</a>
                      </td>
                      <td>
                      <a href="/{{$pokemon->name}}">See more</a>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>

          @else
              <span>No results</span>
          @endif

          </div>
        </div>
    </div>
@endsection
