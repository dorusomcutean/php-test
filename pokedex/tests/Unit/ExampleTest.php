<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\RequestException;

class ExampleTest extends TestCase
{
    protected $apiClient;

    protected $mockHandler;

    protected function setUp():void
    {
        $mock = new MockHandler([
            new Response(200, []),
            new Response(404, []),
            new RequestException("Error Communicating with Server", new Request('GET', 'test'))
        ]);

        $handler = HandlerStack::create($mock);
        $client = new Client(['handler' => $handler]);

        $this->apiClient = $client;
    }
    
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $resp = $this->apiClient->request('GET', '/pokemons');

        $this->assertEquals(200, $resp->getStatusCode());
    }
}
